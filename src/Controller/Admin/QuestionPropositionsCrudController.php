<?php

namespace App\Controller\Admin;

use App\Entity\QuestionPropositions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class QuestionPropositionsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return QuestionPropositions::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->setLabel('Id')->onlyOnIndex();
        yield TextField::new('choice')->setLabel('Choix');
        yield AssociationField::new('question')->setLabel('Question');
        yield TextField::new('value')->setLabel('Valeur')->onlyOnForms();
        yield BooleanField::new('is_answer')->setLabel('Est la bonne réponse ?')->onlyOnForms();
    }
}