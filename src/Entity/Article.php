<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['read:Article']],
            security: 'is_granted("ROLE_USER")'
        ),
        new Post(
            uriTemplate: '/articles/{id}/user_learned',
            denormalizationContext: ['groups' => ['learn:Article']],
            security: 'is_granted("ROLE_USER")',
        ),
    ]
)]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:ReglesArticles', 'read:regles', 'read:Article'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:ReglesArticles', 'read:regles', 'read:Article', 'read:Qcm', 'read:qcm:answer'])]
    private ?string $libelle = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['read:ReglesArticles', 'read:Article'])]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:ReglesArticles', 'read:Article'])]
    private ?string $photo = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'articles')]
    #[Groups(['read:ReglesArticles', 'read:Article', 'learn:Article', 'read:test'])]
    private Collection $user_learned;

    #[ORM\ManyToOne(inversedBy: 'articles')]
    #[Groups(['read:Qcm'])]
    private ?Regles $regles = null;

    #[ORM\ManyToMany(targetEntity: self::class)]
    #[Groups(['read:regles', 'read:Article'])]
    private Collection $childrens;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    #[Groups(['read:regles', 'read:Article'])]
    private ?bool $is_main_article = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read:regles', 'read:Article'])]
    private ?int $time = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:regles'])]
    private ?string $slug = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Groups(['read:regles', 'read:Article'])]
    private ?int $position = null;

    #[Groups(['read:regles', 'read:Article', 'read:test'])]
    private ?bool $is_learned = false;

    #[Groups(['read:regles', 'read:Article', 'read:test'])]
    private ?int $nb_of_child_leanrned;

    #[ORM\OneToMany(mappedBy: 'questionArticle', targetEntity: Question::class)]
    private Collection $questions;

    public function __construct()
    {
        $this->user_learned = new ArrayCollection();
        $this->childrens = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUserLearned(): Collection
    {
        return $this->user_learned;
    }

    public function addUserLearned(User $userLearned): self
    {
        if (!$this->user_learned->contains($userLearned)) {
            $this->user_learned->add($userLearned);
        }

        return $this;
    }

    public function removeUserLearned(User $userLearned): self
    {
        $this->user_learned->removeElement($userLearned);

        return $this;
    }

    public function getRegles(): ?Regles
    {
        return $this->regles;
    }

    public function setRegles(?Regles $regles): self
    {
        $this->regles = $regles;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function addChildren(self $children): self
    {
        if (!$this->childrens->contains($children)) {
            $this->childrens->add($children);
        }

        return $this;
    }

    public function removeChildren(self $children): self
    {
        $this->childrens->removeElement($children);

        return $this;
    }

    public function isIsMainArticle(): ?bool
    {
        return $this->is_main_article;
    }

    public function setIsMainArticle(bool $is_main_article): self
    {
        $this->is_main_article = $is_main_article;

        return $this;
    }

    public function __toString(): string
    {
        return $this->libelle;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(?int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function setIsLearned(?bool $is_learned): self{
        $this->is_learned = $is_learned;
        return $this;
    }

    public function getIsLearned(): ?bool{
        return $this->is_learned;
    }

    public function getNbOfChildLeanrned(): ?int{
        return $this->nb_of_child_leanrned;
    }

    public function setNbOfChildLeanrned(?int $nb_of_child_leanrned): self{
        $this->nb_of_child_leanrned = $nb_of_child_leanrned;
        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setQuestionArticle($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getQuestionArticle() === $this) {
                $question->setQuestionArticle(null);
            }
        }

        return $this;
    }
}
