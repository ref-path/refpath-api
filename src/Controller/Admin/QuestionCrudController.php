<?php

namespace App\Controller\Admin;

use App\Entity\Question;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class QuestionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Question::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->setLabel('Id')->onlyOnIndex();
        yield TextField::new('question_name')->setLabel('Nom de la question');
        yield TextEditorField::new('question_text')->setLabel('Texte de la question')->onlyOnForms();
        yield TextEditorField::new('feedback')->setLabel('Feedback')->onlyOnForms();
        yield AssociationField::new('questionFile')->setLabel('Fichiers');
        yield AssociationField::new('questionArticle')->setLabel('Article');
    }
}