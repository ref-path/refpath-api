<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserDashBoardController extends AbstractCrudController
{
    private UserPasswordHasherInterface $passwordEncoder;

    public function configureActions(Actions $actions): Actions
    {
        parent::configureActions($actions);

/*        $allowUser = Action::new('allowUser', 'Autoriser')
            ->displayIf(
                static function (User $user) {
                    return !$user->isVerified() && $user->getStatus() === User::$NOUVEL_UTILISATEUR;
                }
            )
            ->linkToCrudAction('allowUser');
        $blockUser = Action::new('blockUser', 'Bloquer')
            ->displayIf(
                static function (User $user) {
                    return $user->isVerified();
                }
            )
            ->linkToCrudAction('blockUser');
        $unblockUser = Action::new('unblockUser', 'Débloquer')
            ->displayIf(
                static function (User $user) {
                    return !$user->isVerified() && $user->getStatus() === User::$UTILISATEUR_BLOQUE;
                }
            )
            ->linkToCrudAction('unblockUser');*/

        return $actions;
/*            ->add(Crud::PAGE_INDEX, $allowUser)
            ->add(Crud::PAGE_INDEX, $blockUser)
            ->add(Crud::PAGE_INDEX, $unblockUser)*/
/*            ->add(Crud::PAGE_DETAIL, $allowUser)
            ->add(Crud::PAGE_DETAIL, $blockUser)
            ->add(Crud::PAGE_DETAIL, $unblockUser);*/
    }

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addPanel('Données arbitre')->setIcon('fa fa-user');
        yield EmailField::new('email')->onlyWhenUpdating()->setDisabled();
        yield EmailField::new('email')->onlyWhenCreating();
        yield TextField::new('email')->onlyOnIndex();
//        yield BooleanField::new('isVerified', 'Activation');
//        yield ChoiceField::new('status')->renderAsBadges([
//            User::$NOUVEL_UTILISATEUR => 'info',
//            User::$UTILISATEUR_VALIDE => 'success',
//            User::$UTILISATEUR_BLOQUE => 'danger',
//            User::$UTILISATEUR_SEND_MAIL => 'warning',
//        ])
//            ->setChoices(
//                [
//                    User::$NOUVEL_UTILISATEUR => User::$NOUVEL_UTILISATEUR,
//                    User::$UTILISATEUR_VALIDE => User::$UTILISATEUR_VALIDE,
//                    User::$UTILISATEUR_BLOQUE => User::$UTILISATEUR_BLOQUE,
//                    User::$UTILISATEUR_SEND_MAIL => User::$UTILISATEUR_SEND_MAIL,
//                ]
//            );

//        yield DateField::new('date_inscription')->onlyOnIndex();
        $roles = ['ROLE_SUPERADMIN', 'ROLE_ADMIN', 'ROLE_ARBITRE','ROLE_USER'];
        yield ChoiceField::new('roles')
            ->setChoices(array_combine($roles, $roles))
            ->allowMultipleChoices()
            ->renderAsBadges();
        yield FormField::addPanel('Changer le mot de passe')->setIcon('fa fa-key');
        yield Field::new('password', 'New password')->onlyWhenCreating()->setRequired(true)
            ->setFormType(RepeatedType::class)
            ->setFormTypeOptions([
                'type' => PasswordType::class,
                'first_options' => ['label' => 'New password'],
                'second_options' => ['label' => 'Repeat password'],
                'error_bubbling' => true,
                'invalid_message' => 'The password fields do not match.',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de de passe doit contenir au moins {{ limit }} caractères.',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ]
            ]);
        yield Field::new('password', 'New password')->onlyWhenUpdating()->setRequired(false)
            ->setFormType(RepeatedType::class)
            ->setFormTypeOptions([
                'type' => PasswordType::class,
                'first_options' => ['label' => 'New password'],
                'second_options' => ['label' => 'Repeat password'],
                'error_bubbling' => true,
                'invalid_message' => 'The password fields do not match.',
                'constraints' => [
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de de passe doit contenir au moins {{ limit }} caractères.',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ]
            ]);

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        parent::updateEntity($entityManager, $entityInstance);
    }

    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);

        $first = null;
        $second = null;
        $request = current($context->getRequest()->request);
        if(!empty($request)
            && !empty($request['User'])
            && !empty($request['User']['password']['first'])
            && !empty($request['User']['password']['second'])
        ) {
            $first = $request['User']['password']['first'];
            $second = $request['User']['password']['second'];
        }
        $this->addEncodePasswordEventListener($formBuilder, $first, $second);


        return $formBuilder;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $this->addEncodePasswordEventListener($formBuilder);

        return $formBuilder;
    }

    protected function addEncodePasswordEventListener(FormBuilderInterface $formBuilder, $first = null, $second = null): void
    {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($first, $second) {
            if(!empty($first) && !empty($second) && $first === $second) {
                /** @var User $user */
                $user = $event->getData();
                $hashedPassword = $this->passwordEncoder->hashPassword($user, $first);
                if ($user->getPassword() !== $hashedPassword)
                {
                    $user->setPassword($hashedPassword);
                }
            }
        });
    }
}