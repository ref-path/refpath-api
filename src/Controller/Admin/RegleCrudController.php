<?php

namespace App\Controller\Admin;

use App\Entity\Regles;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class RegleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Regles::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->setLabel('Id')->onlyOnIndex();
        yield TextField::new('libelle')->setLabel('Nom de la règle');
        yield SlugField::new('slug')->setLabel('Slug')->onlyOnForms()->setTargetFieldName('libelle');
        yield TextField::new('photo')->setLabel('Photo');
    }
}