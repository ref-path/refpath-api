<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Repository\QcmRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: QcmRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            uriTemplate: '/qcm/generate',
            normalizationContext: [
                'groups' => ['qcm:generation'],
            ],
            denormalizationContext: [
                'groups' => ['qcm:generation'],
            ],
            security: 'is_granted("ROLE_USER")',
        ),

        new Get(
            uriTemplate: '/qcm/{id}',
            requirements: [
                'uuid' => '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$',
            ],
            normalizationContext: [
                'groups' => ['read:Qcm'],
            ],
            security: 'is_granted("ROLE_USER")',
        ),

        new GetCollection(
            uriTemplate: '/qcms',
            paginationEnabled: true,
            paginationItemsPerPage: 3,
            order: ['id' => 'DESC'],
            normalizationContext: [
                'groups' => ['read:Qcm'],
            ],
            security: 'is_granted("ROLE_USER")',
        ),

        new GetCollection(
            uriTemplate: '/qcms/all',
            order: ['id' => 'ASC'],
            normalizationContext: [
                'groups' => ['read:Qcm'],
            ],
            security: 'is_granted("ROLE_USER")',
        ),
    ]
)]
class Qcm
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(['qcm:generation', 'read:Qcm'])]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Groups(['read:Qcm'])]
    private ?DateTimeInterface $date = null;

    #[ORM\Column(nullable: true, options: ['default' => 0])]
    #[Groups(['read:Qcm'])]
    private ?int $note = null;

    #[ORM\ManyToOne(inversedBy: 'qcms')]
    private ?User $user = null;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    #[Groups(['read:Qcm'])]
    private ?bool $is_completed = null;

    #[ORM\Column(length: 3, nullable: true, options: ['default' => 0])]
    #[Groups(['read:Qcm'])]
    private ?int $current_question = null;

    #[ORM\OneToMany(mappedBy: 'qcm', targetEntity: QcmQuestion::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read:Qcm'])]
    private Collection $questions;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function isCompleted(): ?bool
    {
        return $this->is_completed;
    }

    public function setIsCompleted(bool $is_completed): self
    {
        $this->is_completed = $is_completed;

        return $this;
    }

    /**
     * @return Collection<int, QcmQuestion>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(QcmQuestion $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setQcm($this);
        }

        return $this;
    }

    public function removeQuestion(QcmQuestion $question): self
    {
        if ($this->questions->removeElement($question)) {
            if ($question->getQcm() === $this) {
                $question->setQcm(null);
            }
        }

        return $this;
    }

    public function getCurrentQuestion(): ?int
    {
        return $this->current_question;
    }

    public function setCurrentQuestion(?int $current_question): self
    {
        $this->current_question = $current_question;

        return $this;
    }
}
