<?php

namespace App\ApiResource\Denormalizer;

use App\Entity\Article;
use App\Entity\Qcm;
use App\Entity\QcmQuestion;
use App\Entity\Question;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;

class ArticleDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    private Security $security;
    private EntityManagerInterface $entityManager;

    private const ALREADY_CALLED = 'ARTICLE_DENORMALIZER_ALREADY_CALLED';

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $type === Article::class;
    }

    /**
     * @throws ExceptionInterface
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        $article = $this->denormalizer->denormalize($data, $type, $format, $context);
        $article->addUserLearned($this->security->getUser());

        return $article;
    }
}
