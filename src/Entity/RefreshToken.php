<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'refresh_token')]
class RefreshToken extends \Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken
{
}
