<?php

namespace App\ApiResource\Denormalizer;

use App\Entity\Qcm;
use App\Entity\QcmQuestion;
use App\Entity\Question;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;

class QcmDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    private Security $security;
    private EntityManagerInterface $entityManager;

    private const ALREADY_CALLED = 'QCM_DENORMALIZER_ALREADY_CALLED';

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $type === Qcm::class;
    }

    /**
     * @throws ExceptionInterface
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        $qcm = $this->denormalizer->denormalize($data, $type, $format, $context);
        $qcm->setDate(new DateTime());
        $qcm->setUser($this->security->getUser());
        $qcm->setIsCompleted(false);
        $qcm->setCurrentQuestion(0);
        $qcm->setNote(0);

        $questionRepository = $this->entityManager->getRepository(Question::class);
        $randomQuestions = $questionRepository->getRandomQuestions(20);

        foreach ($randomQuestions as $question) {
            $qcmQuestion = new QcmQuestion();
            $qcmQuestion->setQuestion($question);
            $qcmQuestion->setQcm($qcm);
            $qcm->addQuestion($qcmQuestion);
        }

        return $qcm;
    }
}
