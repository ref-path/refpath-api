<?php

namespace App\ApiResource\Serializer;

use App\Entity\Qcm;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class QcmNormalizer implements ContextAwareNormalizerInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'QCM_NORMALIZER_ALREADY_CALLED';

    protected $normalizer;
    private Security $security;
    private RequestStack $requestStack;

    public function __construct(Security $security, NormalizerInterface $normalizer, RequestStack $requestStack)
    {
        $this->normalizer = $normalizer;
        $this->security = $security;
        $this->requestStack = $requestStack;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Qcm;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        //$user = $this->security->getUser();

        return $this->normalizer->normalize($object, $format, $context);
    }
}