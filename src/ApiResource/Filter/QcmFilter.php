<?php

namespace App\ApiResource\Filter;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\Qcm;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;

class QcmFilter implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        $user = $this->security->getUser();
        $rootAlias = $queryBuilder->getRootAliases()[0];

        if (Qcm::class === $resourceClass) {
            $queryBuilder->andWhere(sprintf('%s.user = :user', $rootAlias));
            $queryBuilder->andWhere(sprintf('%s.is_completed = :is_completed', $rootAlias));
            $queryBuilder->setParameter('user', $user);
            $queryBuilder->setParameter('is_completed', true);
        }
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, Operation $operation = null, array $context = []): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $user = $this->security->getUser();

        if (Qcm::class === $resourceClass) {
            $queryBuilder->andWhere(sprintf('%s.user = :user', $rootAlias));
            $queryBuilder->setParameter('user', $user);
        }
    }
}

