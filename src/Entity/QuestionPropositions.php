<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\QuestionPropositionsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QuestionPropositionsRepository::class)]
#[ApiResource(
    operations: []
)]
class QuestionPropositions
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['read:Qcm'])]
    private ?string $choice = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read:Qcm', 'read:qcm:answer'])]
    private ?string $value = null;

    #[ORM\Column]
    #[Groups(['read:qcm:answer'])]
    private ?bool $is_answer = null;

    #[ORM\ManyToOne(inversedBy: 'questionPropositions')]
    private ?Question $question = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChoice(): ?string
    {
        return $this->choice;
    }

    public function setChoice(string $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function isIsAnswer(): ?bool
    {
        return $this->is_answer;
    }

    public function setIsAnswer(bool $is_answer): self
    {
        $this->is_answer = $is_answer;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }
}
