<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\ArticleController;
use App\Repository\ReglesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReglesRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/regles/',
            paginationEnabled: false,
            normalizationContext: ['groups' => ['read:regles']],
            security: 'is_granted("ROLE_USER")'
        ),
        new Get(
            uriTemplate: '/regles/{id}',
            normalizationContext: ['groups' => ['read:ReglesArticles']],
            security: 'is_granted("ROLE_USER")',
        ),
    ]
)]
class Regles
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:ReglesArticles', 'read:regles', 'read:test'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:ReglesArticles', 'read:regles', 'read:test', 'read:Qcm'])]
    private ?string $libelle = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:ReglesArticles', 'read:regles'])]
    private ?string $photo = null;

    #[ORM\OneToMany(mappedBy: 'regles', targetEntity: Article::class)]
    #[Groups(['read:ReglesArticles', 'read:regles', 'read:test'])]
    private Collection $articles;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $slug = null;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles->add($article);
            $article->setRegles($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getRegles() === $this) {
                $article->setRegles(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->libelle;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
