<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Repository\QcmQuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QcmQuestionRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            uriTemplate: '/qcm/answer',
            normalizationContext: [
                'groups' => ['read:qcm:answer'],
            ],
            denormalizationContext: [
                'groups' => ['qcm:answer'],
            ],
            security: 'is_granted("ROLE_USER")',
        ),
    ]
)]
class QcmQuestion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['qcm:answer'])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'questions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?qcm $qcm = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:Qcm', 'read:qcm:answer'])]
    private ?Question $question = null;

    #[ORM\OneToMany(mappedBy: 'QcmQuestion', targetEntity: Answer::class, cascade: ['persist'])]
    private Collection $answers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQcm(): ?qcm
    {
        return $this->qcm;
    }

    public function setQcm(?qcm $qcm): self
    {
        $this->qcm = $qcm;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Collection<int, Answer>
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(Answer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers->add($answer);
            $answer->setQcmQuestion($this);
        }

        return $this;
    }

    public function removeAnswer(Answer $answer): self
    {
        if ($this->answers->removeElement($answer)) {
            // set the owning side to null (unless already changed)
            if ($answer->getQcmQuestion() === $this) {
                $answer->setQcmQuestion(null);
            }
        }

        return $this;
    }
}
