<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArticleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->setLabel('Id')->onlyOnIndex();
        yield TextField::new('libelle')->setLabel('Nom de l\'article');
        yield SlugField::new('slug')->setLabel('Slug')->onlyOnForms()->setTargetFieldName('libelle');
        yield TextEditorField::new('description')->setLabel('Description de l\'article')->onlyOnForms();
        yield AssociationField::new('regles')->setLabel('Règle');
        yield TextField::new('photo')->setLabel('Photo')->onlyOnForms();
        yield BooleanField::new('is_main_article')->setLabel('Article principale ?')->onlyOnForms();
        yield AssociationField::new('childrens')->setLabel('Articles enfants')->onlyOnForms();
        yield IntegerField::new('time')->setLabel('Temps de lecture');
        yield IntegerField::new('position')->setLabel('Position');
    }
}