<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\Question;
use App\Entity\QuestionFile;
use App\Entity\QuestionPropositions;
use App\Entity\Regles;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $routeBuilder = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(UserDashBoardController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()->setTitle('Ref\'Path');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Accueil');
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::section('Règles & Articles')->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Règles', 'fa fa-star', Regles::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Articles', 'fa fa-book', Article::class)->setPermission('ROLE_ADMIN');

        yield MenuItem::section('QCM & Questions')->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Fichiers des questions', 'fa fa-file', QuestionFile::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Questions', 'fa fa-question', Question::class)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Propositions de réponses', 'fa fa-list', QuestionPropositions::class)->setPermission('ROLE_ADMIN');

        yield MenuItem::section('Utilisateurs')->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Utilisateur', 'fa fa-user', User::class)->setPermission('ROLE_ADMIN');
    }
}
