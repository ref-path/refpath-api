<?php

namespace App\ApiResource\Serializer;

use App\Entity\Article;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ArticleNormalizer implements ContextAwareNormalizerInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'ARTICLE_NORMALIZER_ALREADY_CALLED';

    protected $normalizer;
    private Security $security;

    public function __construct(Security $security, NormalizerInterface $normalizer)
    {
        $this->normalizer = $normalizer;
        $this->security = $security;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Article;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($object, $format, $context);

        $user = $this->security->getUser();

        if ($user instanceof User) {
            $is_learned = $object->getUserLearned()->contains($user);
            $data['is_learned'] = $is_learned;
        } else {
            $data['is_learned'] = false;
        }

        $nb_of_child_learned = 0;
        foreach ($object->getChildrens() as $child) {
            if ($child->getUserLearned()->contains($user)) {
                $nb_of_child_learned++;
            }
        }

        $data['nb_of_child_learned'] = $nb_of_child_learned;

        return $data;
    }
}