<?php

namespace App\ApiResource\Denormalizer;

use App\Entity\Qcm;
use App\Entity\QcmQuestion;
use App\Entity\Answer;
use App\Entity\Question;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;

class QcmQuestionDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    private Security $security;
    private EntityManagerInterface $entityManager;

    private const ALREADY_CALLED = 'QCM_QUESTION_DENORMALIZER_ALREADY_CALLED';

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $type === QcmQuestion::class;
    }

    /**
     * @throws ExceptionInterface
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        $user = $this->security->getUser();

        $questionId = $data['question_id'] ?? null;
        if ($questionId === null) {
            throw new BadRequestHttpException('The "question_id" field is missing.');
        }

        $question = $this->entityManager->getRepository(Question::class)->find($questionId);
        if (!$question) {
            throw new NotFoundHttpException('The specified question was not found.');
        }

        $qcmId = $data['qcm_id'] ?? null;
        if ($qcmId === null) {
            throw new BadRequestHttpException('The "qcm_id" field is missing.');
        }

        $qcm = $this->entityManager->getRepository(Qcm::class)->find($qcmId);
        if (!$qcm) {
            throw new NotFoundHttpException('The specified Qcm was not found.');
        }

        $qcmQuestion = $this->entityManager->getRepository(QcmQuestion::class)->findOneBy([
            'question' => $question,
            'qcm' => $qcm,
        ]);

        if ($qcmQuestion && $qcmQuestion->getQcm()->getUser() === $user) {
            if (isset($data['answers']) && is_array($data['answers'])) {
                foreach ($data['answers'] as $answerData) {
                    $answer = new Answer();
                    $answer->setQcmQuestion($qcmQuestion);
                    $answer->setValue($answerData);
                    $answer->setTime(new DateTime());

                    $qcmQuestion->addAnswer($answer);
                }

                $qcm->setCurrentQuestion($qcm->getCurrentQuestion() + 1);

                $propostions = $qcmQuestion->getQuestion()->getQuestionPropositions();
                $hasIncorrectAnswer = false;

                foreach ($propostions as $answerValue){
                    if($answerValue->isIsAnswer() === true && !in_array($answerValue->getValue(), $data['answers'])){
                        $hasIncorrectAnswer = true;
                    }
                }
                if (!$hasIncorrectAnswer) {
                    $qcm->setNote($qcm->getNote() + 1);
                }

                if ($qcm->getCurrentQuestion() === $qcm->getQuestions()->count()) {
                    $qcm->setIsCompleted(true);
                }
            }
        } else {
            throw new AccessDeniedHttpException('You do not have permission to modify this QCM question.');
        }

        return $qcmQuestion;
    }
}
