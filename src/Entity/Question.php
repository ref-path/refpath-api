<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QuestionRepository::class)]
#[ApiResource(
    operations: []
)]
class Question
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:Qcm'])]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['read:Qcm'])]
    private ?string $question_name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['read:Qcm'])]
    private ?string $question_text = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['read:Qcm', 'read:qcm:answer'])]
    private ?string $feedback = null;

    #[ORM\ManyToOne(inversedBy: 'question')]
    #[Groups(['read:Qcm'])]
    private ?QuestionFile $questionFile = null;

    #[ORM\ManyToOne(inversedBy: 'questions')]
    #[Groups(['read:Qcm', 'read:qcm:answer'])]
    private ?Article $questionArticle = null;

    #[ORM\OneToMany(mappedBy: 'question', targetEntity: QuestionPropositions::class)]
    #[Groups(['read:Qcm', 'read:qcm:answer'])]
    private Collection $questionPropositions;

    public function __construct()
    {
        $this->questionPropositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestionName(): ?string
    {
        return $this->question_name;
    }

    public function setQuestionName(string $question_name): self
    {
        $this->question_name = $question_name;

        return $this;
    }

    public function getQuestionText(): ?string
    {
        return $this->question_text;
    }

    public function setQuestionText(?string $question_text): self
    {
        $this->question_text = $question_text;

        return $this;
    }

    public function getFeedback(): ?string
    {
        return $this->feedback;
    }

    public function setFeedback(?string $feedback): self
    {
        $this->feedback = $feedback;

        return $this;
    }

    public function getQuestionFile(): ?QuestionFile
    {
        return $this->questionFile;
    }

    public function setQuestionFile(?QuestionFile $questionFile): self
    {
        $this->questionFile = $questionFile;

        return $this;
    }

    public function getQuestionArticle(): ?Article
    {
        return $this->questionArticle;
    }

    public function setQuestionArticle(?Article $questionArticle): self
    {
        $this->questionArticle = $questionArticle;

        return $this;
    }

    /**
     * @return Collection<int, QuestionPropositions>
     */
    public function getQuestionPropositions(): Collection
    {
        return $this->questionPropositions;
    }

    public function addQuestionProposition(QuestionPropositions $questionProposition): self
    {
        if (!$this->questionPropositions->contains($questionProposition)) {
            $this->questionPropositions->add($questionProposition);
            $questionProposition->setQuestion($this);
        }

        return $this;
    }

    public function removeQuestionProposition(QuestionPropositions $questionProposition): self
    {
        if ($this->questionPropositions->removeElement($questionProposition)) {
            // set the owning side to null (unless already changed)
            if ($questionProposition->getQuestion() === $this) {
                $questionProposition->setQuestion(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->question_name;
    }
}
