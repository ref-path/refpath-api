<?php

namespace App\Controller\Admin;

use App\Entity\QuestionFile;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Validator\Constraints\File;

class QuestionFileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return QuestionFile::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->setLabel('Id')->onlyOnIndex();
        yield TextField::new('name')->setLabel('Nom du fichier');
        yield TextField::new('file_url')->setLabel('Fichier (Photo / Vidéos)')->onlyOnForms();

        //yield ImageField::new('file_url')
        //    ->setBasePath('uploads/question_file')
        //    ->setUploadDir('public/uploads/question_file')
        //    ->setUploadedFileNamePattern('[slug]-[timestamp].[extension]');

        yield ChoiceField::new('type')->renderAsBadges([
            QuestionFile::$TYPE_PHOTO => 'info',
            QuestionFile::$TYPE_VIDEO => 'info',
        ])->setChoices([
            QuestionFile::$TYPE_PHOTO => 'Photo',
            QuestionFile::$TYPE_VIDEO => 'Vidéo',
        ])->setLabel('Type de fichier');
    }
}